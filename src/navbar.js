import Component from './component.js';

import './navbar.css';

export default class Navbar extends Component {
  static getRootClass() {
    return '.navbar';
  }

  constructor(root) {
    super(root);

    let selectedBtn = root.querySelector('#hard');
    root.querySelectorAll('.modebtn').forEach((modebtn) => {
      modebtn.onclick = (e) => {
        let mode = e.target.id;
        this.fire('modeChange', mode);
        e.target.classList.toggle('selected');
        selectedBtn.classList.toggle('selected');
        selectedBtn = e.target;
      };
    });
    /** @type {Object.<string, HTMLButtonElement>} */
    this.modeButtons = Object.fromEntries(
      [...root.querySelectorAll('.modebtn')].map((modebtn) => {
        return [modebtn.id, modebtn];
      })
    );
  }

  clickMode(mode) {
    this.modeButtons[mode].click();
  }
}
