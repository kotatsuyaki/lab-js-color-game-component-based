import Component from './component.js';

import './card.css';

/*
 * [Event name: params]
 * click: this, index
 */
export default class Card extends Component {
  static getRootClass() {
    return '.card';
  }

  constructor(root, index) {
    super(root);

    /** @type {number} */
    this.index = index;
    console.log('root:', root);
    root.addEventListener('click', () => this.fire('click', this.index));
  }

  fadeOut() {
    this.root.style.opacity = 0;
  }

  fadeIn(color) {
    this.root.style.backgroundColor = '#' + color;
    this.root.style.opacity = 1;
  }
}
