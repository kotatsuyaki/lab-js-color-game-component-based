import Component from './component.js';

import './reset.css';

/*
 * [Event name: params]
 * click: this
 */
export default class Reset extends Component {
  static getRootClass() {
    return '.reset';
  }

  constructor(root) {
    super(root);

    root.addEventListener('click', () => this.fire('click'));
    this.resetDisplay = root.querySelector('.reset span');
    this.showNewColor();
  }

  showNewColor() {
    this.resetDisplay.textContent = 'New Color';
  }

  showPlayAgain() {
    this.resetDisplay.textContent = 'Play Again';
  }
}
